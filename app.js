const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")


// Initialize dotenv
require('dotenv').config()
// dotenv.config()

const app = express()
const port = process.env.PORT || 8001

// MongoDB connection

mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0.qk2mc8i.mongodb.net/booking-system-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.once('open', () => console.log("Connected to MongoDB"))

// MongoDB connection End

// To avoid CORS errors when trying to send request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))


// Routes
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)
// Routes End

app.listen(port, () => console.log(`API is now running on localhost:${port}`))


